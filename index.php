<?php
/**
 * @author      Webjump Core Team <dev@webjump.com>
 * @copyright   2016 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 *
 * @link        http://www.webjump.com.br
 *
 */
require_once("vendor/autoload.php");

// use Webjump\Obramax\Exemples\DataRequest\Order;
use Webjump\Obramax\Pagador\Transaction\ObramaxFacade;


$facade = new ObramaxFacade();

function getData($response)
{
    if (! is_object($response)) {
        return '';
    }

    $result = [];
    foreach (get_class_methods($response) as $method) {
        if ($method === '__construct') {
            continue;
        }
        $result[$method] = $response->$method();
    }

    return $result;
}

//##
//## Boleto
//##
//echo '<h1>Boleto</h1>';
//$data = new Billet();
$response = $facade->sendBillet($data);
//
if (is_object($response)) {
   echo "<a target='_blank' href='{$response->getPaymentUrl()}'>Gerar Boleto</a>";
   echo '<pre>';
   // print_r($response);
   // echo '</pre>';
} else {
   echo '<pre>';
   print_r($response);
   echo '</pre>';

}

echo '<hr />';
