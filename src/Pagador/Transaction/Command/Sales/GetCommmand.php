<?php
/**
 * @author      Webjump Core Team <dev@webjump.com>
 * @copyright   2016 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 *
 * @link        http://www.webjump.com.br
 *
 */

namespace Webjump\Obramax\Pagador\Transaction\Command\Sales;

use Webjump\Obramax\Factories\ClientHttpFactory;
use Webjump\Obramax\Factories\ResponseFactory;
use Webjump\Obramax\Factories\SalesFactory;
use Webjump\Obramax\Pagador\Transaction\Command\CommandAbstract;
use Webjump\Obramax\Pagador\Transaction\Api\Order\Send\RequestInterface as OrderCardData;


class GetCommand extends CommandAbstract
{
	protected function execute()
	{
		$sales = SalesFactory::make($this->request);
		$client = ClientHttpFactory::make();

		$params = $this->request->getParams();
		$uriComplememt = $params['uriComplememt']['payment_id'];

		$response = $client->request($sales, 'GET', $uriComplememt);

		$type = '';
		if ($this->request()->getType()) {
			$type = $thos->request->getType();
		}

		$this->result = ResponseFactory::make($this->getResponseToArray($response), $type);
	}
}