<?php
/**
 * @author      Webjump Core Team <dev@webjump.com>
 * @copyright   2016 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 *
 * @link        http://www.webjump.com.br
 *
 */

namespace Webjump\Obramax\Pagador\Transaction\Command\Sales;

use Webjump\Obramax\Factories\ClientHttpFactory;
use Webjump\Obramax\Factories\ResponseFactory;
use Webjump\Obramax\Factories\SalesFactory;
use Webjump\Obramax\Pagador\Transaction\Command\CommandAbstract;


class VoidCommand extends CommandAbstract
{
	protected function execute()
	{
		$sales = SalesFactory::make($this->request);
		$client = ClientHttpFactory::make();

		$params = $this->request->getParams();
		$uriComplement = sprintf('%s/void', $params['uriComplement']['payment_id']);

		if (isset($params['uriComplement']['additional']) && !empty($params['uriComplement']['additional']) && is_array($params['uriComplement']['additional'])) {
			$uriComplement .= '?' . \http_build_query($params['uriComplement']['additional']);
		}

		$response = $client->request($sales, 'PUT', $uriComplement);

		$this->result = ResponseFactory::make($this->getResponseToArray($response), $type);
	}
}