<?php
/**
 * @author      Webjump Core Team <dev@webjump.com>
 * @copyright   2016 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 *
 * @link        http://www.webjump.com.br
 *
 */

namespace Webjump\Obramax\Pagador\Transaction\Command;

use Webjump\Obramax\Factories\ClientHttpFactory;
use Webjump\Obramax\Factories\ResponseFactory;
use Webjump\Obramax\Factories\SalesFactory;
use Webjump\Obramax\Pagador\Transaction\Resource\Order\Send\Request as OrderRequest;


class SaleCommand extends CommandAbstract
{
	protected function make()
	{
		$sales = SalesFactory::make($this->request);
		$client = ClientHttpFactory::make();

		$response = $client->request($sales);

		$type = '';

		if ($this->request instanceof OrderRequest) {
			$type = ResponseFactory::CLASS_TYPE_ORDER;
		}

		$this->result = ResponseFactory::make($this->getResponseToArray($response), $type);
	}
}