<?php
/**
 * @author      Webjump Core Team <dev@webjump.com>
 * @copyright   2016 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 *
 * @link        http://www.webjump.com.br
 *
 */

namespace Webjump\Obramax\Pagador\Transaction\Api\Billet\Send;

use Webjump\Obramax\Pagador\Transaction\Api\AuthRequestInterface;


interface RequestInterface extends AuthRequestInterface
{
	public function getCodeEnteprise(); // codigo da empresa

	public function getCodeOfficeSale(); // codigo do escritorio de vendas

	public function getNameOfficeSale(); // nome do escritorio de vendas

	public function getNumberOrder(); // numero do pedido

	public function getNumberOrderOrigin(); // numero pedido origem

	public function getIdReserveOrder(); // id de reserva do pedido

	public function getTypeOrder(); // tipo do pedido

	public function getDateOrder(); // data do pedido

	public function getValueOrder(); // valor do pedido

	public function getValueFreightTotalWithoutDiscount(); // valor do frete total sem desconto

	public function getValueFreightTotalWithDiscount(); // valor do frete total com desconto

	public function getCodeReasonDiscountFreightTotal(); // codigo motivo desconto frete total

	public function getCodeUserDiscountFreightTotal(); // codigo user desconto frete total

	public function getReasonFreightAdditional(); // motivo frete adicional

	public function getCodeCurrency(); // codigo da moeda

	public function getObservationGerenalFiscal(); // observação geral fiscal

	public function getObservationGeneralOrder(); // observação geral pedido

	public function getOrganizationSeller(); // organização vendas

	public function getChanelDistribution(); // canal distribuição

	public function getSectorActivity(); // setor de ativiade

	public function getGroupSellers();  // grupo vendendores

	public function getChanelSellers(); // canal de vendas

	public function getNumberDocumentOrigin(); // numero do documento de origem

	public function getCodeClientHybris(); // codigo do client hybris

	public function getCodeClientDoc(); // codigo do cliente doc

	public function getTypeClient(); // tipo de cliente

	public function getTypeDocumentionClient(); // tipo de documento

	public function getDocumentClient(); // documento cliente

	public function getRegistrationClient(); // matricula

	public function getNameClient(); // nome do cliente

	public function getNameFantasyClient(); // nome  fantasia

	public function getRegistrationMunicipalClient(); // matricula municipal

	public function getRegistrationStateClient(); // matricula estadual

	public function getCodeSectorIndustrialClient(); // codigo setor industrial

	public function getCodeLanguageClient(); // codigo do idioma

	public function getPhoneCommercialClient(); // telefone comercial

	public function getPhoneResidentialClient(); // telefone residencial

	public function getCellPhoneClient(); // telefone celular

	public function getFaxOneClient(); // fax 1

	public function getFaxTwoClient(); // fax 2

	public function getEmailClient(); // email

	public function getEmailNfeClient(); // email nfe

	public function getTypeAddress();// tipoo de endereço

	public function getIdAddressBucAddress(); // id do endereço buc

	public function getPublicPlaceAddress(); // logradouro

	public function getNumberAddress(); // numero do endereço

	public function getComplementAddress(); // complemento do endereço

	public function getNeighborhoodAddress(); // bairro do endereço

	public function getCityAddress(); // cidade do endereço

	public function getStateAddress(); // estado do endereço

	public function getCountryAddress(); // pais do endereço

	public function getZipCodeAddress(); // cep do endereço

	public function getCodeIBGEAddress(); // codigo do IBGE

	public function getCodeProduct(); // codigo do produto

	public function getLotProduct(); // lote do produto

	public function getSeqItemOrderProduct(); // sequencia do pedido

	public function getIdReserveItemProduct(); // id de reserva do produto

	public function getCodeCentralStockProduct(); // codigo central do estoque

	public function getCodeDepositStockProduct(); // codigo de deposito do produto

	public function getCodeCentralExpeditionProduct(); // codigo de expedição do produto

	public function getCodeDepositExpeditionProduct(); // codigo de deposito de expedição do produto

	public function getLocalExpeditionSAPProduct(); // local de expedição do produto

	public function getCodeSallerProduct(); // codigo do vendedor

	public function getAmountProduct(); // quantidade do produto

	public function getUnitMeasureProduct(); // unidade de medida

	public function getValueUnitProduct(); // valor unitario

	public function getValorFinalProduct(); // valor final

	public function getCodeReasonDiscountAdministeredProduct(); // codigo do motivo de desconto do administrador

	public function getCodeReasonDiscountPunctualProduct(); // codigo do motivo de desconto pontual

	public function getCodeReasonDiscountCampaignProduct(); // codigo do motivo de desconto da companha

	public function getValueDiscountUnitAdministered(); // valor do desconto unitario administrado

	public function getValueDiscountUnitPunctualProduct(); // valor do desconto unitario pontual

	public function getValueDiscountUnitCampaignProduct(); // valor do desconto unitario da companha

	public function getPriceNormalProduct(); // preço normal do produto

	public function getPriceSpecialProduct(); // preço espcial do produto

	public function getFlowExitProduct(); // fluxo de saido do produto

	public function getFlagOrderProduct(); // flag da encomenda

	public function getValueFreightProduct(); // valor do frete

	public function getDateDeliveryProduct(); // data de entrega

	public function getShiftDeliveryProduct(); // turno de entrega

	public function getGrossWeightProduct(); // peso bruto do prduto

	public function getNetWeightProduct(); // peso liquido do produto

	public function getFlagReserveOfficeProduct(); // flag de reversa do pruduto

	public function getTypeProcessingProduct(); // tipo de processamento do produto

	public function getValueExpenseAccessoryProduct(); // valor da despesa de acessoria do produto

	public function getSeqItemParentProduct(); // sequencia do item associado

	public function getNumberOrderPurchaseItemProduct(); // numero da compra do produto

	public function getSeqItemPedPurchaseProduct(); // sequencia do pedido do item

	public function getObservationGeneralItemProduct(); // observação geral do item do produto
}