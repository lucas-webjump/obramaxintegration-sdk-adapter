<?php
/**
 * @author      Webjump Core Team <dev@webjump.com>
 * @copyright   2016 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 *
 * @link        http://www.webjump.com.br
 *
 */

namespace Webjump\Obramax\Pagador\Transaction;

use Webjump\Obramax\Factories\OrderRequestFactory;
use Webjump\Obramax\Pagador\Transaction\Api\Order\Send\RequestInterface as OrderRequest;
use Webjump\Obramax\Factories\SalesCommandFactory;
use Webjump\Obramax\Pagador\Transaction\Command\Sales\CaptureCommand;
use Webjump\Obramax\Pagador\Transaction\Command\Sales\GetCommand;
use Webjump\Obramax\Pagador\Transaction\Command\Sales\VoidCommand;
use Webjump\Obramax\Pagador\Transaction\Command\SalesCommand;


class ObramaxFacade implements FacedeInterface
{
	/**
	 * @param OrderRequest $request
	 * @return SalesCommand
	 */
	public function sendOrder(OrderRequest $request)
	{
		return SalesCommandFactory::make(OrderRequestFactory::make($request))->getResult();
	}
}