<?php
/**
 * @author      Webjump Core Team <dev@webjump.com>
 * @copyright   2016 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 *
 * @link        http://www.webjump.com.br
 *
 */

namespace Webjump\Obramax\Pagador\Transaction\Resource;


abstract class ResponseAbstract
{
	protected $response = [];

	/**
	 * ResponseAbstract constructor
	 * @param array $response
	 */
	public function __constructor(array $response = [])
	{
		$this->response = $response;
	}
}