<?php
/**
 * @author      Webjump Core Team <dev@webjump.com>
 * @copyright   2016 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 *
 * @link        http://www.webjump.com.br
 *
 */

namespace Webjump\Obramax\Pagador\Transaction\Resource\Order\Send;

use Webjump\Obramax\Pagador\Transaction\Resource\RequestAbstract;
use Webjump\Obramax\Pagador\Transaction\Api\Billet\Send\RequestInterface as Data;


class Request extends RequestAbstract
{
	/**
	 * @param Data $data
	 */
	public function __construct(Data $data)
	{
		$this->data = $data;
		$this->prepareParams();
	}

	/**
	 * @return $this
	 */
	public function prepareParams()
	{
		$this->params = [
			'headers' => [
				'Content-Type' => self::CONTENT_TYPE_APPLICATION_XML,
				'Authorization' => $this->data->getBasicAuth()
			],
			'pedido_venda' => [
				'codigo_empresa' => '',
				'codigo_escritorio' => '',
				'nome_escritorio' => '',
				'numero_pedido' => '',
				'numero_pedido_origem' => '',
				'id_reserva_pedido' => '',
				'tipo_pedido' => '',
				'data_pedido' => '',
				'valor_pedido' => '',
				'valor_frete_total_sem_desconto' => '',
				'valor_frete_total_cem_desconto' => '',
				'codigo_motivo_desconto_frete_total' => '',
				'codigo_usuario_desconto_frete_total' => '',
				'motivo_frete_adicional' => '',
				'codigo_moeda' => '',
				'observacao_geral_fiscal' => '',
				'observacao_geral_pedido' => '',
				'organizacao_vendas' => '',
				'canal_distribuicao' => '',
				'setor_atividade' => '',
				'grupo_vendedores' => '',
				'canal_vendas' => '',
				'numero_documento_origem' => '',
				'lista_cliente' => [
					'cliente' => [
						'codigo_cliente_hybris' => '',
						'codigo_cliente_buc' => '',
						'tipo_cliente' => '',
						'tipo_documento' => '',
						'documento_cliente' => '',
						'matricula' => '',
						'nome_cliente' => '',
						'nome_fantasia' => '',
						'forma_tratamento' => '',
						'inscricao_municipal' => '',
						'inscricao_estadual' => '',
						'codigo_setor_industrial' => '',
						'codigo_idioma' => '',
						'telefone_comercial' => '',
						'telefone_residencial' => '',
						'telefone_celular' => '',
						'fax1' => '',
						'fax2' => '',
						'email' => '',
						'email_nfe' => '',
						'endereco' => [
							'tipo_endereco' => '',
							'id_endereco' => '',
							'logradouro' => '',
							'numero' => '',
							'bairro' => '',
							'cidade' => '',
							'estado' => '',
							'pais' => '',
							'CEP' => '',
							'codigo_IBGE' => ''
						]
					]
				],
				'lista_produto' => [
					'produto' => [
						'codigo_produto' => '',
						'lote' => '',
						'seq_item_pedido' => '',
						'id_reserva_item' => '',
						'codigo_centro_estoque' => '',
						'codigo_deposito_estoque' => '',
						'codigo_centro_expedicao' => '',
						'codigo_deposito_expedicao' => '',
						'local_expedicao_sap' => '',
						'codigo_vendedor' => '',
						'quantidade_produto' => '',
						'unidade_medida' => '',
						'valor_unitario' => '',
						'valor_final' => '',
						'codigo_motivo_desconto_administrado' => '',
						'codigo_motivo_desconto_pontual' => '',
						'codigo_motivo_desconto_campanha' => '',
						'valor_desconto_unitario_administrado' => '',
						'valor_desconto_unitario_pontual' => '',
						'valor_desconto_unitario_campanha' => '',
						'preco_normal' => '',
						'preco_especial' => '',
						'fluxo_saida' => '',
						'flag_encmomenda' => '',
						'valor_frete' => '',
						'data_entrega' => '',
						'turno_entrega' => '',
						'peso_bruto' => '',
						'peso_liquido' => '',
						'flag_reserva_offline' => '',
						'tipo_processamento' => '',
						'valor_despesa_acessoria' => '',
						'seq_item_associado' => '',
						'num_pedido_compra_item' => '',
						'seq_pedido_ped_compra' => '',
						'observacao_geral_item' => ''
					]
				]
			],
		];

		return $this;
	}
}