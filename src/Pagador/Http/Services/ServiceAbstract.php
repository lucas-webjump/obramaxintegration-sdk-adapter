<?php
/**
 * @author      Webjump Core Team <dev@webjump.com>
 * @copyright   2016 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 *
 * @link        http://www.webjump.com.br
 *
 */

namespace Webjump\Obramax\Pagador\Http\Services;

use Webjump\Obramax\Transaction\Resource\RequestAbstract;


abstract class ServiceAbstract
{
	protected $endPoint = '';
	protected $request;

	/**
	 * @param  RequestAbstract $request
	 */
	public function __constructor(RequestAbstract $request)
	{
		$this->request = $request;
	}

	/**
	 * @return  sring
	 */
	public function getEndPoint()
	{
		return $this->endPoint;
	}

	public function setEndPoint($endPoint)
	{
		$this->endPoint = $endPoint;
		return $this;
	}

	/**
	 * @return RequestAbstract
	 */
	public function getRequest()
	{
		return $this->request;
	}
}