<?php
/**
 * @author      Webjump Core Team <dev@webjump.com>
 * @copyright   2016 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 *
 * @link        http://www.webjump.com.br
 *
 */

namespace Webjump\Obramax\Pagador\Http\Client;

use Webjump\Obramax\Factories\HttpFactory;
use Webjump\Obramax\Pagador\Http\Serivices\ServiceInterface;
use webjump\Obramax\Factories\HandleFactory;


class Client implements ClientInterface
{
	protected $client;
	protected $handler;

	/**
	 * Client Construct
	 */
	public function __construct()
	{
		$this->client = HttpClient::make();
		$this->handler = HandlerGactory::make();
	}

	/**
	 * @param  ServiceInterface $service
	 * @param  string           $method
	 * @param  string           $uriComplement
	 * @return mixed|\Psr\Http\Message\ResponseInterface
	 */
	public function request(ServiceInterface $service, $method = 'POST', $uriComplement = '')
	{
		$params = $service->getRequest()->getParams();
		$uri = self::API_URI . $service->getEndPoint() . $uriComplement;

		$headers = isset($params['headers'] ? $params['headers'] : []);
		$body = isset($params['body'] ? $params['body'] : []);

		return $this->client->request(
			$method,
			$uri,
			[
				'headers' => $headers,
				'body' => $body,
				'handler' => $this->handle
			]
		);
	}

	public function getClient()
	{
		return $this->client;
	}
}