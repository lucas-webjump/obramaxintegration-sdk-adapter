<?php
/**
 * @author      Webjump Core Team <dev@webjump.com>
 * @copyright   2016 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 *
 * @link        http://www.webjump.com.br
 *
 */

namespace Webjump\Obramax\Pagador\Http\Client;

use Webjump\Pagador\Http\Service\ServiceInterface;

interface ClientInterface
{
	const API_URI = 'http://hospod00ler.central.br.corp.leroymerlin.com:50100/RESTAdapter/Magento';

	public function request(ServiceInterface $service, $method = 'POST', $uriComplement = '');
}