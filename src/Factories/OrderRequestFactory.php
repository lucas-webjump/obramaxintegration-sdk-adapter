<?php
/**
 * @author      Webjump Core Team <dev@webjump.com>
 * @copyright   2016 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 *
 * @link        http://www.webjump.com.br
 *
 */

namespace Webjump\Obramax\Factories;

use Webjump\Obramax\Pagador\Transaction\Resource\Order\Send\Request;
use Webjump\Obramax\Pagador\Transaction\Api\Order\Send\RequestInterface as Data;


class OrderRequestFactory
{
    /**
     * @param Data $data
     * @return Request
     */
    public static function make(Data $data)
    {
        return new Request($data);
    }
}