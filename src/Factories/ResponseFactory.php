<?php
/**
 * @author      Webjump Core Team <dev@webjump.com>
 * @copyright   2016 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 *
 * @link        http://www.webjump.com.br
 *
 */

namespace Webjump\Obramax\Factories;

use Webjump\Obramax\Pagador\Transaction\Resource\Order\Send\Response as OrderResponse;
use Webjump\Obramax\Pagador\Transaction\Resource\Actions\Response as ActionsResponse;

class ResponseFactory
{
	const CLASS_TYPE_ORDER = 'order';
	const CLASS_TYPE_ACTIONS = 'actions';

	public static function make(array $data, $type)
    {
        if ($type === self::CLASS_TYPE_ORDER) {
            return new BilletResponse($data);
        }

        if ($type === self::CLASS_TYPE_ACTIONS) {
            return new ActionsResponse($data);
        }
    }
}