<?php
/**
 * @author      Webjump Core Team <dev@webjump.com>
 * @copyright   2016 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 *
 * @link        http://www.webjump.com.br
 *
 */

namespace Webjump\Obramax\Examples\DataRequest;

use Webjump\Obramax\Pagador\Transaction\Api\Order\Send\RequestInterface;


class Order implements RequestInterface
{
	// Implements all methods necessary to operation of send order SAP PO

	public function getCodeEnterprise()
	{
		return 'BM01';
	}

	public function getCodeOfficeSale()
	{
		return 'BM01';
	}
}